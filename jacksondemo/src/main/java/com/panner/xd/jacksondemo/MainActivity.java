package com.panner.xd.jacksondemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private ObjectMapper objectMapper;
    private String s;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,false);
        try {
            objectMapper.writeValue(new File("xx.json"),People.class);
            //得到json字符串
            People people = objectMapper.readValue(new File("xx.json"), People.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public String javaToJson(People people){
        try {
            objectMapper.writeValue(new File("xx"),people);
            s = objectMapper.writeValueAsString(people);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }
}
