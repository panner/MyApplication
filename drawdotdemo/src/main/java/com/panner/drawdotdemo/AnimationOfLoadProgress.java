package com.panner.drawdotdemo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

/**
 * @author panzhijie
 * @version 2017-05-22 17:27
 */
public class AnimationOfLoadProgress extends View {

    private Paint bgPaint;
    private Paint prePaint;
    private int dp_10;

    public AnimationOfLoadProgress(Context context) {
        this(context, null);
    }

    public AnimationOfLoadProgress(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnimationOfLoadProgress(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AnimationOfLoadProgress(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        int ws = (width - dp_10) / 3;
        int start = 0;
        Log.e("width+height",String.valueOf(width)+String.valueOf(height));
        do {
            canvas.drawRect(start, 0, start + dp_10, dp_10, bgPaint);
            start += ws;
        } while (start < width - dp_10);
    }

    private void init() {
        bgPaint = new Paint();
        bgPaint.setStyle(Paint.Style.FILL);
        bgPaint.setColor(Color.parseColor("#aeaeae"));
        bgPaint.setAntiAlias(true);//抗锯齿

        prePaint = new Paint();
        prePaint.setStyle(Paint.Style.FILL);
        prePaint.setColor(getResources().getColor(R.color.colorAccent));
        prePaint.setAntiAlias(true);//抗锯齿

        dp_10 = DensityUtils.dp2px(getContext(), 10);
    }
}
