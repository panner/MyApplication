package com.panner.drawdotdemo;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    private AnimationOfLoadProgress sanimation;
    private ProgressBar progress;
    int postate = 0;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progress = (ProgressBar) findViewById(R.id.progress);
        progress.setMax(100);
        //new Thread(new Runnable() {
        //    @Override
        //    public void run() {
        //
        //        handler.postDelayed(new Runnable() {
        //            @Override
        //            public void run() {
        //                progress.setProgress(postate);
        //                postate++;
        //            }
        //        }, 1000);
        //    }
        //}).start();
        //sanimation = (AnimationOfLoadProgress) findViewById(R.id.animation_load);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(0, 100);
        valueAnimator.setDuration(3000);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (int) animation.getAnimatedValue();
                progress.setProgress(value);
                //sanimation.layout(value,value,value+sanimation.getWidth(),value+sanimation.getHeight());
            }

        });
        //valueAnimator.addListener(new Animator.AnimatorListener() {
        //    @Override
        //    public void onAnimationStart(Animator animation) {
        //
        //    }
        //
        //    @Override
        //    public void onAnimationEnd(Animator animation) {
        //
        //    }
        //
        //    @Override
        //    public void onAnimationCancel(Animator animation) {
        //
        //    }
        //
        //    @Override
        //    public void onAnimationRepeat(Animator animation) {
        //
        //    }
        //});
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);//无限循环
        valueAnimator.start();
    }
}
