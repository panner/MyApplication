package com.panner.rxjavademo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

public class FirstActivity extends AppCompatActivity {

    private ArrayList<Person> list;
    private TextView textAge;
    private static final String TAG = "FirstActivity";
    private ArrayList<Person> persons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        list = new ArrayList<>();
        for (int i = 18; i < 30; i++) {
            list.add(new Person("xiaoming", "7.7", i));
        }

        textAge = (TextView) findViewById(R.id.text_age);
        method();
    }

    public void method() {
        persons = new ArrayList<>();

        Observable.from(list).map(new Func1<Person, Person>() {
            @Override
            public Person call(Person person) {
                return person;
            }
        }).subscribe(new Action1<Person>() {
            @Override
            public void call(Person person) {
                Log.e("person_age....", String.valueOf(person.getAge()));
                textAge.setText(String.valueOf(person.getAge()));

                Person people = new Person("haha", "7.8", person.getAge());
                persons.add(people);
                Log.e("peole;;;;", persons.toString());
            }
        });


        Observable.from(new Future<Person>() {

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            @Override
            public boolean isCancelled() {
                return false;
            }

            @Override
            public boolean isDone() {
                return false;
            }

            @Override
            public Person get() throws InterruptedException, ExecutionException {
                return null;
            }

            @Override
            public Person get(long timeout, @NonNull TimeUnit unit)
                    throws InterruptedException, ExecutionException, TimeoutException {
                return null;
            }
        });
    }
}
