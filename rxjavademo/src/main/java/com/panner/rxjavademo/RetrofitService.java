package com.panner.rxjavademo;

import retrofit2.Call;
import retrofit2.http.POST;

/**
 * @author panzhijie
 * @version 2017-04-24 16:13
 */
public interface RetrofitService {
    @POST("/data")
    Call loadData();
}
