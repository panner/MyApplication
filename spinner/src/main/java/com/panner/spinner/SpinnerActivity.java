package com.panner.spinner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class SpinnerActivity extends AppCompatActivity {
    private ArrayList mList = new ArrayList<String>();
    private Spinner mSpinner;
    private ArrayAdapter<String> mAdapter;
    private TextView mText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        mSpinner = (Spinner) findViewById(R.id.spinner_city);
        mText = (TextView) findViewById(R.id.text_city);
        //步骤一：添加下拉表项的列表
        mList.add("重庆");
        mList.add("深圳");
        mList.add("香港");
        mList.add("美国");
        //步骤二：为下拉菜单定义适配器，定义样式，传入list
        mAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,mList);
        //步骤三：将适配器添加到spinner上
        mSpinner.setAdapter(mAdapter);
        //步骤四：选中条目的处理
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mText.setText(mAdapter.getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /**
         * 弹出框选项的触摸事件的处理
         */
        mSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
        /*
        下拉菜单对弹出内容的选项焦点处理
         */
        mSpinner.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });
    }
}
