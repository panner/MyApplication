package com.panner.xd.myfeedbackdemo;

import android.app.Application;
import com.alibaba.sdk.android.feedback.impl.FeedbackAPI;

/**
 * @author panzhijie
 * @version 2017-01-23 14:40
 */
public class MyApplication extends Application {
    @Override public void onCreate() {
        super.onCreate();
        FeedbackAPI.init(this,"23615822");
    }
}
