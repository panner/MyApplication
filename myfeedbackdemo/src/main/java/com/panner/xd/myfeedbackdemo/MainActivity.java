package com.panner.xd.myfeedbackdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.alibaba.sdk.android.feedback.impl.FeedbackAPI;

public class MainActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FeedbackAPI.openFeedbackActivity();
    }
}
