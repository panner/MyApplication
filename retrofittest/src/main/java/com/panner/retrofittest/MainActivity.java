package com.panner.retrofittest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        requestNet();
    }

    @OnClick(R.id.btn_resquest_net)
    void requestNet() {
        //retrofit的基本使用
        Retrofit retrofit = new Retrofit.Builder().baseUrl("www.baidu.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())//RxJava
                .build();
        NetService service = retrofit.create(NetService.class);
        Call<List<String>> call = service.getList("abc");
        try {
            //同步调用
            Response<List<String>> response = call.execute();
            int size = response.body().size();
            Log.e("返回的列表长度", String.valueOf(size));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //异步请求
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                int size = response.body().size();
                Log.e("返回的列表长度", String.valueOf(size));
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                Log.i("请求失败", t.getMessage());
            }
        });

        //移除请求
        call.cancel();


        //rxjava+retrofit结合
        Observable<List<String>> observable = service.get("123", "abc");
        observable.subscribeOn(Schedulers.io()).subscribe(new Subscriber<List<String>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<String> strings) {

            }
        });
    }

}
