package com.panner.retrofittest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

/**
 * @author panzhijie
 * @version 2017-07-06 14:26
 */
public interface NetService {
    @POST("/list")
    Call<List<String>> getList(@Body String s);

    @GET("/repos/{owner}/{repo}/contributors")
    Observable<List<String>> get(@Path("owner") String owner, @Path("repo") String repo);
}
