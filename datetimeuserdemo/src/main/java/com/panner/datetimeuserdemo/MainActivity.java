package com.panner.datetimeuserdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.orhanobut.logger.Logger;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Days;
import org.joda.time.Duration;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import java.sql.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //DateTime dateTime = new DateTime();
        //Logger.d(dateTime.toDateTime());
        //
        //System.out.println("dateTime.getYear()"+dateTime.getYear());
        //System.out.println("year()"+dateTime.year().toString());
        //System.out.println("getYearOfEra()"+dateTime.getYearOfEra());
        //DateTime dateTime1 = new DateTime(2017, 4, 27, 0, 0, 0);
        //Logger.init("带参数 datetime");
        //Logger.d(dateTime1.toDateTime());
        //
        //DateTime dateTime2 = new DateTime("2017-04-27");
        //Logger.init("2017-04-27");
        //Logger.d(dateTime2);
        //
        //LocalDate localDate = new LocalDate(2017, 4, 27);
        //Logger.init("localdate");
        //Logger.d(localDate);
        //
        //LocalTime dateTime3 = new LocalTime(13, 59, 10, 10);
        //Logger.d(dateTime3);
        DateTime dateTime1 = DateTime.parse("14:03", DateTimeFormat.forPattern("HH:mm"));
        DateTime dateTime2 = DateTime.parse("15:03", DateTimeFormat.forPattern("HH:mm"));
        int days = Days.daysBetween(dateTime1, dateTime2).getDays();
        Logger.init("days.........");
        Logger.e(String.valueOf(days));
        Duration duration = new Duration(dateTime2, dateTime1);
        long millis = duration.getMillis();
        Logger.e(String.valueOf(millis));

    }
}
