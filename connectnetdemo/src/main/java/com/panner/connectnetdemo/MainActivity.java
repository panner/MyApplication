package com.panner.connectnetdemo;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

/**
 * 手机获取连接的WiFi和管理练习
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(
                CONNECTIVITY_SERVICE);
        NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
        System.out.println(allNetworkInfo.length);
        for (int i = 0; i < allNetworkInfo.length; i++) {
            System.out.println(allNetworkInfo[i].toString());
        }
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        List<ScanResult> scanResults = wifiManager.getScanResults();
        for (ScanResult scanResult : scanResults) {
            System.out.println(scanResult.toString());
        }
        String ssid = wifiManager.getConnectionInfo().getSSID();
        System.out.println(ssid);
        //try {
        //    wifiManager.wait();
        //} catch (InterruptedException e) {
        //    e.printStackTrace();
        //}
    }
}
