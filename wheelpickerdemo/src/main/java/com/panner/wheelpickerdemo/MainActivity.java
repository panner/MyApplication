package com.panner.wheelpickerdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);

    }

    public List setYearData() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1790; i < 2017; i++) {
            list.add(i);
        }
        return list;
    }

    public List setMonthData() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            list.add(i);
        }
        return list;
    }

    public List setDayData() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 30; i++) {
            list.add(i);
        }
        return list;
    }

    @OnClick(R.id.set_time)
    void setTime() {
        Calendar instance = Calendar.getInstance();
        instance.set(1970, 1, 1);
        TimePickerView build = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {

            }
        }).setType(new boolean[] { true, true, true, false, false, false })
                .setCancelText("cancel")
                .setLabel(null, "Mon", null, null, null, null)
                .setLabelTextSize(28)
                .setContentSize(28)
                .setDate(Calendar.getInstance())
                .setRangDate(instance, Calendar.getInstance())
                .build();
        build.show();
    }
}
