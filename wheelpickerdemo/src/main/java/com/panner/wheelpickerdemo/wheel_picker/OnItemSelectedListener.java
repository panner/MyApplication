package com.panner.wheelpickerdemo.wheel_picker;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
