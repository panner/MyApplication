package com.panner.wheelpickerdemo.wheel_picker;

/**
 * Created by Sai on 15/8/9.
 */
public interface OnDismissListener {
    public void onDismiss(Object o);
}
