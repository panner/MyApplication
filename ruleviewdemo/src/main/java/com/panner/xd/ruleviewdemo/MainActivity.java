package com.panner.xd.ruleviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.wx.wheelview.widget.WheelView;

public class MainActivity extends AppCompatActivity {

    private ScrollingValuePicker picker;
    private static final float MIN_VALUE = 100;
    private static final float MAX_VALUE = 215;
    private static final float LINE_RULER_MULTIPLE_SIZE = 3.5f;
    private TextView text;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        picker = (ScrollingValuePicker) findViewById(R.id.picker);
        text = (TextView) findViewById(R.id.tv_message);
        initRulerView();
    }
    private void initRulerView() {
        picker.setViewMultipleSize(LINE_RULER_MULTIPLE_SIZE);
        picker.setMaxValue(MIN_VALUE, MAX_VALUE);
        picker.setValueTypeMultiple(12);
        picker.getScrollView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    picker.getScrollView().startScrollerTask();
                }
                return false;
            }
        });
        text.setText("DWRulerView Value : " +170);
        picker.setOnScrollChangedListener(new ObservableHorizontalScrollView.OnScrollChangedListener() {

            @Override
            public void onScrollChanged(ObservableHorizontalScrollView view, int l, int t) {
            }

            @Override
            public void onScrollStopped(int l, int t) {
                text.setText("DWRulerView Value : " +
                                  DWUtils.getValueAndScrollItemToCenter(picker.getScrollView()
                                          , l
                                          , t
                                          , MAX_VALUE
                                          , MIN_VALUE
                                          , picker.getViewMultipleSize()));
            }
        });
    }
}
