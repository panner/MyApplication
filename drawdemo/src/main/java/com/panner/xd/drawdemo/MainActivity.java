package com.panner.xd.drawdemo;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //init();
        final MyTestView view = (MyTestView) findViewById(R.id.text_view);
        Button button = (Button) findViewById(R.id.button);
        view.setText("over");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        float x = view.getX();
        float y = view.getY();
        int top = view.getTop();
        int bottom = view.getBottom();
        int left = view.getLeft();
        int right = view.getRight();
        Log.e("x.y.top.bottom.left.r", x + "::::" + y + ":::::" + top + ":::" + bottom + "::::" + left + ":::" + right);
        Toast.makeText(getApplicationContext(), String.valueOf(bottom), Toast.LENGTH_SHORT).show();

        //速度追踪，用于追踪手指在滑动的过程中的速度
        VelocityTracker obtain = VelocityTracker.obtain();
        obtain.computeCurrentVelocity(3000);
        float xVelocity = obtain.getXVelocity();//水平方向的速度
        float yVelocity = obtain.getYVelocity();//垂直方向的速度

        obtain.clear();
        obtain.recycle();

        /*手势检测 GestureDetector
        * 单击、滑动、长按、双击等行为
        * */
        GestureDetector gestureDetector = new GestureDetector(new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            //拖动
            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            //快速滑动
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });

        gestureDetector.setIsLongpressEnabled(false);//解决长按屏幕后无法拖动的现象
//        view.scrollBy(10, 20);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.smoothScrollTo(100,100);
            }
        });

    }

    private void init() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.root);
        final DrawView view = new DrawView(this);
        view.setMinimumHeight(500);
        view.setMinimumWidth(300);
        //通知view组件重绘
        view.invalidate();
        layout.addView(view);

    }

    public void startAlarm(AlarmManager mAlamManager, PendingIntent pi) {
        //设置定时任务触发的时间
        Calendar c = Calendar.getInstance();
        //c.set(Calendar.HOUR_OF_DAY,alarmInfo.getHour());
        //c.set(Calendar.MINUTE,alarmInfo.getMinute());
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        //  Log.d("alarm", "当前系统版本" + Build.VERSION.SDK_INT);
        if (c.getTimeInMillis() < System.currentTimeMillis()) {
            if (Build.VERSION.SDK_INT >= 19) {
                mAlamManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis() + 24 * 60 * 60 * 1000, pi);
            } else {
                mAlamManager.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis() + 24 * 60 * 60 * 1000, pi);
            }
        } else {
            if (Build.VERSION.SDK_INT >= 19) {
                mAlamManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pi);
            } else {
                mAlamManager.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pi);
            }
        }
    }
}
