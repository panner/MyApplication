package com.example.colorpickerview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;

public class ColorPicker extends View {

    private static final float RADIUS_WIDTH_RATIO = 0.5f;
    private Paint mTouchViewPaint;

    private Paint mPickViewPaint;    // 绘制色盘
    private Bitmap mBitmapView;    // 彩灯位图
    private int mCenterX, mCenterY;
    private int mPickViewRadius;
    private Rect mColorWheelRect;
    private int mTouchCircleX;

    private int mTouchCircleY;
    private Context mContext;
    private float[] colorHSV = new float[]{0f, 1f, 1f};
    private OnSeekColorListener onSeekColorListener;

    public ColorPicker(Context context) {
        this(context, null);
    }

    public ColorPicker(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ColorPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init();
    }

    public void init() {
        setLayerType(LAYER_TYPE_SOFTWARE, null);
        mTouchViewPaint = new Paint();//触摸移动的小圆点画笔
        mTouchViewPaint.setStyle(Paint.Style.STROKE);
        mTouchViewPaint.setColor(Color.WHITE);
        mTouchViewPaint.setStrokeWidth(3);
        mTouchViewPaint.setAntiAlias(true);
        mTouchViewPaint.setDither(true);

        mPickViewPaint = new Paint();
        mPickViewPaint.setAntiAlias(true);//消除锯齿
        mPickViewPaint.setDither(true);//防抖动
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        Matrix m = new Matrix();
//        m.setScale(-1,1);
//        int width=mBitmapView.getWidth();
//        int height=mBitmapView.getHeight();
//        Bitmap reversePic = Bitmap.createBitmap(mBitmapView, 0, 0, width, height, m, true);
        canvas.drawBitmap(mBitmapView, mColorWheelRect.left, mColorWheelRect.top, null);
        canvas.drawCircle(mTouchCircleX, mTouchCircleY, 20, mTouchViewPaint);//画圆点
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldw, int oldh) {
        mCenterX = width / 2;
        mCenterY = height / 2-100;
        mTouchCircleX = mCenterX;
        mTouchCircleY = mCenterY;
        mPickViewRadius = Math.min(mCenterX, mCenterY);
        mColorWheelRect = new Rect(mCenterX - mPickViewRadius, 0,
                mCenterX + mPickViewRadius, mCenterY + mPickViewRadius);
        mBitmapView = createColorWheelBitmap(mPickViewRadius * 2, height);
    }

    private Bitmap createColorWheelBitmap(int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        int colorCount = 12;
        int colorAngleStep = 360 / 12;
        int colors[] = new int[colorCount + 1];
        float hsv[] = new float[]{0f, 1f, 1f};
        for (int i = 0; i < colors.length; i++) {
            hsv[0] = /*360 -*/ (i * colorAngleStep +90) % 360;
            colors[i] = Color.HSVToColor(hsv);
        }

        SweepGradient sweepGradient = new SweepGradient(mCenterX, mCenterY, colors, null);
        RadialGradient radialGradient = new RadialGradient(mCenterX, mCenterY, mPickViewRadius,
                0xFFFFFFFF, 0x00FFFFFF, Shader.TileMode.CLAMP);
        ComposeShader composeShader = new ComposeShader(sweepGradient, radialGradient, PorterDuff.Mode.SRC_OVER);

        mPickViewPaint.setShader(composeShader);

        Canvas canvas = new Canvas(bitmap);
        canvas.drawRect(0, 0, width, height, mPickViewPaint);
        return bitmap;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        ViewParent parent = getParent();
        if (parent != null)
            parent.requestDisallowInterceptTouchEvent(true);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                int x = (int) event.getX();
                int y = (int) event.getY();
                int cx = x - mCenterX;
                int cy = y - mCenterY;
                double d = Math.sqrt(cx * cx + cy * cy);

                if (d <= mPickViewRadius) {
                    colorHSV[0] = (float) (Math.toDegrees(Math.atan2(cy, cx)) + 90f);
                    Log.e("colorHSV[0]",String.valueOf(colorHSV[0]));
                    colorHSV[1] = Math.max(0f, Math.min(1f, (float) (d / mPickViewRadius)));
                }

                if (onSeekColorListener != null) {
                    mTouchCircleY = y;
                    mTouchCircleX = x;
                    onSeekColorListener.onSeekColorListener(getColor());
                    postInvalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
            default:
                break;
        }
        return true;
    }

    public void setColor(int color) {
        Color.colorToHSV(color, colorHSV);
    }

    public int getColor() {
        return Color.HSVToColor(colorHSV);
    }

    public void setOnSeekColorListener(OnSeekColorListener listener) {
        this.onSeekColorListener = listener;
    }

    public interface OnSeekColorListener {
        void onSeekColorListener(int color);
    }
}