package com.example.colorpickerview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button viewById = findViewById(R.id.open);
        viewById.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BrightViewActivity.class);
                startActivity(intent);
            }
        });
        ColorPicker pickView = findViewById(R.id.pick_view);
        pickView.setOnSeekColorListener(new ColorPicker.OnSeekColorListener() {
            @Override
            public void onSeekColorListener(int color) {
                viewById.setBackgroundColor(color);

                Log.e("哈哈哈===============", String.valueOf(color));
            }
        });

    }
}
