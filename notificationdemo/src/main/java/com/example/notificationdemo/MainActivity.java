package com.example.notificationdemo;

import android.app.Notification;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showNotification(){
        Notification notification = new Notification();
        notification.icon = R.drawable.ic_launcher_foreground;
        notification.tickerText = "hello world";
        notification.when = System.currentTimeMillis();

    }
}
