package com.example.verticalseekbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private SeekBar horiSeekBar = null;
    private TextView horiText = null;

    private VerticalSeekBar verticalSeekBar = null;
    private TextView verticalText = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        horiSeekBar = (SeekBar) findViewById(R.id.horiSeekBar);
        horiText = (TextView) findViewById(R.id.horiText);
        horiSeekBar.setOnSeekBarChangeListener(horiSeekBarListener);

        verticalSeekBar = (VerticalSeekBar) findViewById(R.id.verticalSeekBar);
        verticalText = (TextView) findViewById(R.id.verticalText);
        verticalSeekBar
                .setOnSeekBarChangeListener(verticalSeekBarChangeListener);

    }

    private SeekBar.OnSeekBarChangeListener horiSeekBarListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            horiText.setText(Integer.toString(progress));
            verticalSeekBar.setProgress(progress);

        }
    };

    private SeekBar.OnSeekBarChangeListener verticalSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            verticalText.setText(Integer.toString(progress));

        }
    };
}
