package com.panner.xd.bledemo;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter==null){
            Toast.makeText(getApplicationContext(),"蓝牙不支持",Toast.LENGTH_LONG).show();
            finish();
        }
        if (!defaultAdapter.isEnabled()){
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent,1);
        }
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==1){
            if (resultCode==RESULT_OK){
                Toast.makeText(getApplicationContext(),"蓝牙已经开启",Toast.LENGTH_LONG).show();
            }else if (resultCode==RESULT_CANCELED){
                Toast.makeText(getApplicationContext(),"蓝牙开启取消",Toast.LENGTH_LONG).show();
            }
        }
    }
     class MyAdapter extends RecyclerView.Adapter<MyViewHolder>{

         @Override
         public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
             return null;
         }

         @Override
         public void onBindViewHolder(MyViewHolder holder, int position) {

         }

         @Override
         public int getItemCount() {
             return 0;
         }
     }
    class MyViewHolder extends RecyclerView.ViewHolder{
        public MyViewHolder(View itemView) {
            super(itemView);
        }
    }
}
