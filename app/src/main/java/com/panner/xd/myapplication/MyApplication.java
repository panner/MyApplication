package com.panner.xd.myapplication;

import android.app.Application;

import com.orhanobut.logger.Logger;

/**
 * @author panzhijie
 * @version 2017-04-10 10:22
 */
public class MyApplication extends Application {
    private static String TAG="MYAPPLICATION........";
    @Override
    public void onCreate() {
        Logger.init(TAG);
        super.onCreate();
    }
}
