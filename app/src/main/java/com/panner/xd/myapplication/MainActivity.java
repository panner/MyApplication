package com.panner.xd.myapplication;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.orhanobut.logger.Logger;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Logger.t(MainActivity.class.getSimpleName()).d(getPackageName());
        //Logger.i("大家好，我叫%s，今年%d，很高兴大家来看我的文章！！！", "Jerry", 18);
        //Logger.json(createJson().toString());
        //RxPermissions rxPermissions = new RxPermissions(this);

        //launchAppDetail(getVersionCode(),"");

        //startActivity(new Intent(this, RecycleViewDemo.class));
        ((Button)findViewById(R.id.btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "message", Snackbar.LENGTH_LONG).setAction("action",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(getApplicationContext(), "点击了snackbar", Toast.LENGTH_LONG);
                            }
                        }).show();

            }
        });
    }

    public JSONObject createJson() {
        JSONObject name = new JSONObject();
        try {
            name.put("name", "panner");
            JSONObject address = new JSONObject();
            address.put("country", "China");
            address.put("province", "Chong Qing");
            address.put("address", "渝中");
            name.put("address", address);
            name.put("married", true);
            return name;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 启动到应用商店app详情界面
     *
     * @param appPkg 目标App的包名
     * @param marketPkg 应用商店包名 ,如果为""则由系统弹出应用商店列表供用户选择,否则调转到目标市场的应用详情界面，某些应用商店可能会失败
     */
    public void launchAppDetail(String appPkg, String marketPkg) {
        try {
            if (TextUtils.isEmpty(appPkg)) {
                return;
            }

            Uri uri = Uri.parse("http://play.google.com/store/apps/details?id=" + appPkg);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (!TextUtils.isEmpty(marketPkg)) {
                intent.setPackage(marketPkg);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getVersionCode() {
        // 获取packagemanager的实例
        PackageManager packageManager = getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = null;
        try {
            packInfo = packageManager.getPackageInfo(getPackageName(), 0);
            String version = packInfo.packageName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }
}
