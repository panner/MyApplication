package com.panner.xd.builderpatterndemo.simple_factory;

/**
 * Q7汽车的实体类
 *
 * @author panzhijie
 * @version 2017-07-24 14:13
 */
public class Q7Car implements Car {
    @Override
    public void drive() {
        System.out.println("Q7启动了");
    }

    @Override
    public void selfNavigation() {
        System.out.println("Q7自动导航");
    }
}
