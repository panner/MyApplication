package com.panner.xd.builderpatterndemo.simple_factory;

/**
 * 抽象工厂
 *
 * @author panzhijie
 * @version 2017-07-24 14:04
 */
public interface Factory {
    <T extends Car> T createCar(Class<T> clz);
}
