package com.panner.xd.builderpatterndemo.build;

/**
 * @author panzhijie
 * @version 2017-01-23 10:22
 */
public interface Builder {
    Builder buildPartA();
    Builder buildPartB();
    Builder buildPartC();
    Car getProduct();
}
