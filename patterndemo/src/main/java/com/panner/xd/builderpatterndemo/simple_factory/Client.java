package com.panner.xd.builderpatterndemo.simple_factory;

/**
 * 汽车的生产线
 *
 * @author panzhijie
 * @version 2017-07-24 14:16
 */
public class Client {
    public static void main(String[] args) {
        CarFactory factory = new CarFactory();
        Q3Car q3Car = factory.createCar(Q3Car.class);
        q3Car.drive();
        q3Car.selfNavigation();

        Q5Car q5Car = factory.createCar(Q5Car.class);
        q5Car.drive();
        q5Car.selfNavigation();

        Q7Car q7Car = factory.createCar(Q7Car.class);
        q7Car.selfNavigation();
        q7Car.drive();
    }
}
