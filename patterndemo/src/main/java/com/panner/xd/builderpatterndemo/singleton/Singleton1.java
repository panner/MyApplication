package com.panner.xd.builderpatterndemo.singleton;

/**
 * @author panzhijie
 * @version 2017-07-21 11:40
 */
public class Singleton1 {
    private static Singleton1 uniqueInstance;

    private Singleton1() {

    }

    public static synchronized Singleton1 getInstace() {
        if (uniqueInstance == null) {//uniqueInstance对象为null的时候才去创建
            uniqueInstance = new Singleton1();
        }
        return uniqueInstance;
    }
}
