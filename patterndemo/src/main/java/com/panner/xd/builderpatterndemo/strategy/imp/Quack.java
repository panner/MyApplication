package com.panner.xd.builderpatterndemo.strategy.imp;

/**
 * 实现鸭子呱呱叫的具体类
 *
 * @author panzhijie
 * @version 2017-07-19 11:25
 */
public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("鸭子呱呱叫");
    }
}
