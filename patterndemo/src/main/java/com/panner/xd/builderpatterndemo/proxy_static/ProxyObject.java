package com.panner.xd.builderpatterndemo.proxy_static;

/**
 * 要对原来的方法进行修改，可以在原有的方法上修改，也可以使用代理设计模式
 * Created by Administrator on 2018/9/14 0014.
 */

public class ProxyObject extends AbstractObject {
    //目标对象引用
    private RealObject realObject;

    public ProxyObject(RealObject realObject) {
        this.realObject = realObject;
    }

    @Override
    protected void operation() {
        System.out.println("do something before real operation");
        if (realObject == null) {
            realObject = new RealObject();
        }
        realObject.operation();
        System.out.println("do something after real operation");
    }
}
