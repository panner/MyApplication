package com.panner.xd.builderpatterndemo.strategy.imp;

/**
 * 鸭子叫的行为接口
 *
 * @author panzhijie
 * @version 2017-07-19 11:19
 */
public interface QuackBehavior {
    void quack();
}
