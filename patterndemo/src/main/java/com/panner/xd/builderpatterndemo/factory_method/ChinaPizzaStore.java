package com.panner.xd.builderpatterndemo.factory_method;

/**
 * 中国风pizza
 *
 * @author panzhijie
 * @version 2017-07-24 17:55
 */
public class ChinaPizzaStore extends PizzaStore {
    @Override
    Pizza createPizza(String type) {
        if (type.equals("cheese")) {
            return new ChinaPizza();
        } else {
            return null;
        }
    }
}
