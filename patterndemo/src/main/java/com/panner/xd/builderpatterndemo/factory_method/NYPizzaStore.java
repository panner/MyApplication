package com.panner.xd.builderpatterndemo.factory_method;

/**
 * 纽约pizza
 *
 * @author panzhijie
 * @version 2017-07-24 17:59
 */
public class NYPizzaStore extends PizzaStore {
    @Override
    Pizza createPizza(String type) {
        if (type.equals("cheese")) {
            return new NYPizza();
        } else {
            return null;
        }
    }
}
