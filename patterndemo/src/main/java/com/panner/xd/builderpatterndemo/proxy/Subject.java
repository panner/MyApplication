package com.panner.xd.builderpatterndemo.proxy;

/**
 * 代理类和被代理类都要实现的接口
 * Created by Administrator on 2018/9/14 0014.
 */

public interface Subject {
    void shopping();
}
