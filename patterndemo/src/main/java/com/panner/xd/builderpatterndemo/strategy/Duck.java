package com.panner.xd.builderpatterndemo.strategy;

import com.panner.xd.builderpatterndemo.strategy.imp.FlyBehavior;
import com.panner.xd.builderpatterndemo.strategy.imp.QuackBehavior;

/**
 * 所有鸭子的超类
 *
 * @author panzhijie
 * @version 2017-07-19 09:45
 */
public abstract class Duck {
    ///**
    // * 所有的鸭子都呱呱叫
    // */
    //public void quack() {
    //
    //}
    //
    ///**
    // * 所有鸭子都会游泳
    // */
    //public void swim() {
    //
    //}
    //
    ///**
    // * 鸭子的其他行为，每个鸭子的子类只需要重写这个方法实现自己的行为就可以了
    // */
    //public void display() {

    //}
    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;

    public void performQuack() {
        quackBehavior.quack();
    }

    public void performFly() {
        flyBehavior.fly();
    }

    public void swim() {
        System.out.println("游泳的鸭子");
    }

    public abstract void display();

    /**
     * 提供动态设置飞行行为的方法
     */
    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    /**
     * 提供动态设置鸭子叫的方法
     */
    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }
}
