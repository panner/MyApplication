package com.panner.xd.builderpatterndemo.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 每一个动态代理必须实现InvocationHandler接口
 * Created by Administrator on 2018/9/14 0014.
 */

public class Proxy implements InvocationHandler {
    private Object target;//要代理的真实对象

    public Proxy(Subject target) {
        this.target = target;
    }

    /**
     *
     * @param proxy 真实对象
     * @param method 真实对象的方法
     * @param args 方法的参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return null;
    }
}
