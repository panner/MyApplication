package com.panner.xd.builderpatterndemo.strategy;

import com.panner.xd.builderpatterndemo.strategy.imp.Fly;
import com.panner.xd.builderpatterndemo.strategy.imp.Quack;

/**
 * 绿头鸭的具体行为和特征
 *
 * @author panzhijie
 * @version 2017-07-19 11:22
 */
public class MallardDuck extends Duck {
    public MallardDuck() {
        flyBehavior = new Fly();
        quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
}
