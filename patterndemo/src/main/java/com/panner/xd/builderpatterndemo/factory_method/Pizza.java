package com.panner.xd.builderpatterndemo.factory_method;

import java.util.ArrayList;

/**
 * pizza抽象类，提供pizza的制作方法
 *
 * @author panzhijie
 * @version 2017-07-24 17:37
 */
public abstract class Pizza {
    String name;
    String dough;
    String sauce;
    ArrayList toppings = new ArrayList();

    void prepare() {
        System.out.println("Preparing " + name);
        System.out.println("Tossing dough....");
        System.out.println("Adding sauce....");
        System.out.println("Adding toppings: ");
        for (int i = 0; i < toppings.size(); i++) {
            System.out.println("  " + toppings.get(i));
        }
    }

    void bake() {
        System.out.println("Bake for 25 minutes at 350");
    }

    void cut() {
        System.out.println("Cutting the pizza into diagonal slices");
    }

    void box() {
        System.out.println("Place pizza in official pizzaStore box");
    }

    public String getName() {
        return name;
    }
}
