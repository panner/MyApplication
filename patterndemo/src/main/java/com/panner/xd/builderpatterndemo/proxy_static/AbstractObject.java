package com.panner.xd.builderpatterndemo.proxy_static;

/**抽象对象角色
 * Created by Administrator on 2018/9/14 0014.
 */

public abstract class AbstractObject {
    protected abstract void operation();
}
