package com.panner.xd.builderpatterndemo.factory_method;

/**
 * pizza 商店的抽象类
 *
 * @author panzhijie
 * @version 2017-07-24 17:44
 */
public abstract class PizzaStore {
    /**
     * 制作pizza
     */
    public Pizza orderPizza(String type) {
        Pizza pizza;
        pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }

    /**
     * 抽象得工厂方法
     */
    abstract Pizza createPizza(String type);
}
