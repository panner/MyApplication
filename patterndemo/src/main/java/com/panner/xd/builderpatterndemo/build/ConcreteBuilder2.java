package com.panner.xd.builderpatterndemo.build;

import com.panner.xd.builderpatterndemo.R;
import com.panner.xd.builderpatterndemo.build.Builder;
import com.panner.xd.builderpatterndemo.build.Car;

/**
 * @author panzhijie
 * @version 2017-01-23 11:15
 */
public class ConcreteBuilder2 implements Builder {
    private Car car;

    @Override public Builder buildPartA() {
        car.setWheel("4");
        return this;
    }

    @Override public Builder buildPartB() {
        car.setColor(R.color.colorAccent);
        return this;
    }

    @Override public Builder buildPartC() {
        return null;
    }

    @Override public Car getProduct() {
        return car;
    }
}
