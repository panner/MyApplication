package com.panner.xd.builderpatterndemo.simple_factory;

/**
 * 抽象产品
 *
 * @author panzhijie
 * @version 2017-07-24 14:05
 */
public interface Car {
    void drive();

    //自动导航
    void selfNavigation();
}
