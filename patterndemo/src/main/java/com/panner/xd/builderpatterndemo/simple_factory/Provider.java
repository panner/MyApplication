package com.panner.xd.builderpatterndemo.simple_factory;

/**
 * service provider interface
 *
 * @author panzhijie
 * @version 2017-07-24 10:27
 */
public interface Provider {
    Service newService();
}
