package com.panner.xd.builderpatterndemo.singleton;

/**
 * 单例设计模式
 *
 * @author panzhijie
 * @version 2017-07-21 10:33
 */
public class Singleton {

    private static volatile Singleton uniqueInstance;

    private Singleton() {

    }

    /**
     * 这个方法可以大大减少getInstance（）的时间耗费
     *
     * volatile关键字确保了uniqueInstance变量被初始化的时候多个线程可以正确的处理uniqueInstance变量
     * 使用volatile修饰以后，在一个线程中修改了某个变量的值，在其他线程中立即可见
     */
    public static Singleton getInstance() {
        if (uniqueInstance == null) {//检查实例，如果不存在，进入同步区域
            synchronized (Singleton.class) {
                if (uniqueInstance == null) {//进入区块以后在检查一次，如果还是为null，那么创建实例
                    uniqueInstance = new Singleton();
                }
            }
        }
        return uniqueInstance;
    }
}
