package com.panner.xd.builderpatterndemo.proxy;

/**
 * Created by Administrator on 2018/9/14 0014.
 */

public class Client {
    public static void main(String[] args) {
        Subject man = new Man();
        Proxy p = new Proxy(man);
        //通过java.lang.reflect,newProxyInstance()方法获取真实对象的代理对象
        Subject subject = (Subject) java.lang.reflect.Proxy.newProxyInstance(man.getClass().getClassLoader(), man.getClass().getInterfaces(), p);
        subject.shopping();
        //获取真实对象的代理对象所对应的class对象的名称
        System.out.println(subject.getClass().getName());
    }
}
