package com.panner.xd.builderpatterndemo.simple_factory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author panzhijie
 * @version 2017-07-24 10:28
 */
public class Services {
    //避免直接实例化
    private Services() {

    }

    //maps service names to services
    private static final Map<String, Provider> providers = new ConcurrentHashMap<>();
    public static final String DEFAULT_OROVIDER_NAME = "<def>";

    public static void registerDefaultProvider(Provider p) {
        registerProvider(DEFAULT_OROVIDER_NAME, p);
    }

    /**
     * provider registration api
     */
    public static void registerProvider(String name, Provider p) {
        providers.put(name, p);
    }

    public static Service newInstance() {
        return newInstance(DEFAULT_OROVIDER_NAME);
    }

    //service access api
    private static Service newInstance(String name) {
        Provider p = providers.get(name);
        if (p == null) {
            throw new IllegalArgumentException("No provider registered with name:" + name);
        }
        return p.newService();
    }

}
