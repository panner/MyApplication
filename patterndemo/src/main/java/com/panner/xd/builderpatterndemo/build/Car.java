package com.panner.xd.builderpatterndemo.build;

/**
 * @author panzhijie
 * @version 2017-01-23 10:31
 */
public class Car {
    private String wheel;
    private int color;
    private String door;

    public Car() {
    }

    public String getWheel() {
        return wheel;
    }

    public void setWheel(String wheel) {
        this.wheel = wheel;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getDoor() {
        return door;
    }

    public void setDoor(String door) {
        this.door = door;
    }

    @Override public String toString() {
        return "Car{" +
               "wheel='" + wheel + '\'' +
               ", color=" + color +
               ", door='" + door + '\'' +
               '}';
    }
}
