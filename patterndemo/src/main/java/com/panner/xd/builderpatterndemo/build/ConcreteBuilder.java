package com.panner.xd.builderpatterndemo.build;

import com.panner.xd.builderpatterndemo.build.Builder;
import com.panner.xd.builderpatterndemo.build.Car;

/**
 * builder的具体实现类
 * @author panzhijie
 * @version 2017-01-23 10:27
 */
public class ConcreteBuilder implements Builder {
    private Car car;
    public ConcreteBuilder() {
        car=new Car();
    }

    @Override public Builder buildPartA() {
        car.setColor(1);
        return this;
    }

    @Override public Builder buildPartB() {
        car.setDoor("4");
        return this;
    }

    @Override public Builder buildPartC() {
        car.setWheel("6");
        return this;
    }

    @Override public Car getProduct() {
        return car;
    }
}
