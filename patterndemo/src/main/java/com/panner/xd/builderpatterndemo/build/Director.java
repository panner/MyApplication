package com.panner.xd.builderpatterndemo.build;

import com.panner.xd.builderpatterndemo.build.Builder;

/**
 * 内部实现组成
 * @author panzhijie
 * @version 2017-01-23 10:24
 */
public class Director {
    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    //组成完整的对象
    public void construct() {
        builder.buildPartA();
        builder.buildPartB();
        builder.buildPartC();
    }
}
