package com.panner.xd.builderpatterndemo.strategy.imp;

/**
 * 实现鸭子飞的类
 *
 * @author panzhijie
 * @version 2017-07-19 11:25
 */
public class Fly implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("鸭子飞");
    }
}
