package com.panner.xd.builderpatterndemo.strategy.imp;

/**
 * 鸭子飞行行为接口
 *
 * @author panzhijie
 * @version 2017-07-19 11:18
 */
public interface FlyBehavior {
    void fly();
}
