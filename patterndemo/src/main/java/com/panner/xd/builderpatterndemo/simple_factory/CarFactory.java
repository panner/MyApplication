package com.panner.xd.builderpatterndemo.simple_factory;

/**
 * 具体的工厂
 *
 * @author panzhijie
 * @version 2017-07-24 14:09
 */
public class CarFactory implements Factory {
    @Override
    public <T extends Car> T createCar(Class<T> clz) {
        Car car = null;
        try {
            car = (Car) Class.forName(clz.getName()).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T) car;
    }
}
