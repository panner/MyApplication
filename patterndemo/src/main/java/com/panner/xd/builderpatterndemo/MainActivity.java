package com.panner.xd.builderpatterndemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.panner.xd.builderpatterndemo.build.ConcreteBuilder;
import com.panner.xd.builderpatterndemo.build.Director;

public class MainActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ConcreteBuilder builder = new ConcreteBuilder();
        Director director = new Director(builder);
        //组成完整的产品
        director.construct();
    }
}
