package com.panner.xd.builderpatterndemo.factory_method;

/**
 * 制作pizza的测试
 *
 * @author panzhijie
 * @version 2017-07-24 18:00
 */
public class PizzaTest {
    public static void main(String[] srgs) {
        PizzaStore nyStore = new NYPizzaStore();
        ChinaPizzaStore chinaPizzaStore = new ChinaPizzaStore();

        nyStore.orderPizza("cheese");

        chinaPizzaStore.orderPizza("cheese");
    }
}
