package com.panner.xd.builderpatterndemo.simple_factory;

/**
 * Q3汽车的实体类
 *
 * @author panzhijie
 * @version 2017-07-24 14:13
 */
public class Q3Car implements Car {
    @Override
    public void drive() {
        System.out.println("Q3启动了");
    }

    @Override
    public void selfNavigation() {
        System.out.println("Q3自动导航");
    }
}
