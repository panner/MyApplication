package com.panner.servicedemo;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by Panner on 2018/4/23.
 */

public class ServiceTest extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 必须实现的方法，返回一个IBinder对象，APP通过该对象与服务进行通信
     * @param intent
     * @return
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    /**
     * 服务被启动的时候调用，只会执行一次，以后都不会再执行
     */
    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * 该服务上绑定的所有客户端断开的时候调用
     * @param intent
     * @return
     */
    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    /**
     * 服务被关闭的时候调用，只会执行一次
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class MyBinder extends Binder{

        public int getProgress(){
            return 50;
        }

    }
}
